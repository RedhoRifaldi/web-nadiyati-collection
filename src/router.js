import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Profile from './views/Profile.vue'
import Product from './views/Product.vue'
import DaftarProduct from './views/ListProduct.vue'
import Checkout from './views/Checkout.vue'
import Login from './views/Logins.vue'
import Daftar from './views/Daftar.vue'
import Lacak from './views/Lacak.vue'
import Lupa from './views/Lupa.vue'
import CekEmail from './views/CekEmail.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/product/:id',
      name: 'Product',
      component: Product
    },
    {
      path: '/list/:id',
      name: 'DaftarProduct',
      component: DaftarProduct
    },
    {
      path: '/checkout',
      name: 'Checkout',
      component: Checkout
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/daftar',
      name: 'Daftar',
      component: Daftar
    },
    {
      path: '/lacak',
      name: 'Lacak',
      component: Lacak
    },
    {
      path: '/lupa',
      name: 'Lupa',
      component: Lupa
    },
    {
      path: '/CekEmail',
      name: 'CekEmail',
      component: CekEmail
    }
  ]
})
